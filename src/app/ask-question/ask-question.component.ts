import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { Question } from './../question';
import { User } from '../user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ask-question',
  templateUrl: './ask-question.component.html',
  styleUrls: ['./ask-question.component.css']
})
export class AskQuestionComponent implements OnInit {

  constructor(private userService:UserService , private router:Router) { }

  ngOnInit(): void {
    console.log("data coming form user service to ask question comp")
    this.user=this.userService.giveUserData()
    console.log(this.user.id)
  }
askQuestionDTO={
  userId:0,
  question:'',
  topic:''
}

user=new User()

question: Question | undefined
  askQuestion(data:any) {
    this.askQuestionDTO.question=data.question
    this.askQuestionDTO.userId=this.user.id
    this.askQuestionDTO.topic=data.topic
    this.userService.askQuestion(( this.askQuestionDTO)).subscribe((data)=>{
      this.question=data
      alert("Question Posted ")
      this.router.navigate(['/user'])
    })
    
    }

}


